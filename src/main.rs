#![no_std]
#![no_main]

// use esp32_hal::{clock::ClockControl, peripherals::Peripherals, prelude::*, timer::TimerGroup, Rtc, Delay, hall_sensor::add_one};
// use esp_backtrace as _;
// use esp_println::println;
 

// #[xtensa_lx_rt::entry]
// fn main() -> ! {
//     let peripherals = Peripherals::take();
//     let system = peripherals.DPORT.split();
//     let clocks = ClockControl::boot_defaults(system.clock_control).freeze();
//     let mut delay = Delay::new(&clocks);


//     // Disable the RTC and TIMG watchdog timers
//     let mut rtc = Rtc::new(peripherals.RTC_CNTL);
//     let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
//     let mut wdt0 = timer_group0.wdt;
//     let timer_group1 = TimerGroup::new(peripherals.TIMG1, &clocks);
//     let mut wdt1 = timer_group1.wdt;

//     rtc.rwdt.disable();
//     wdt0.disable();
//     wdt1.disable();

//     let timer_touched_cable = timer_group0.timer0;
//     //timer_touched_cable.start(1000);

//     println!("Hello World");
//     println!("Adding 2 +1 : {}", add_one(2));
//     loop {
//         println!("Hello again");
//         delay.delay_ms(1000u32);

//     }
// }

use esp32_hal::{
    adc::{AdcConfig, Attenuation, ADC1, ADC, ADC2},
    clock::ClockControl,
    gpio::IO,
    peripherals::Peripherals,
    prelude::*,
    timer::TimerGroup,
    Delay,
    Rtc,
};

use esp_backtrace as _;
use esp_println::println;
//use xtensa_lx_rt::entry;

mod gpio;
use gpio::{GpioState, Gpio, GpioT};

#[xtensa_lx_rt::entry]
fn main() -> ! {
    let peripherals = Peripherals::take();
    let mut system = peripherals.DPORT.split();
   // let mut system2 = peripherals.SYSTEM.split();

    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();

    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    let mut wdt = timer_group0.wdt;
    let mut rtc = Rtc::new(peripherals.RTC_CNTL);

    // Disable MWDT and RWDT (Watchdog) flash boot protection
    wdt.disable();
    rtc.rwdt.disable();

    // let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    // // Create ADC instances
    // let analog = peripherals.SENS.split();
    // // let analog = peripherals.APB_SARADC.split();

    // // let mut adc2_config = AdcConfig::new();
    // let mut adc1_config = AdcConfig::new();
    // let mut pin =
    //     adc1_config.enable_pin(io.pins.gpio33.into_analog(), Attenuation::Attenuation11dB);
    // let mut adc1 = ADC::<ADC1>::adc(
    //     analog.adc1,
    //     adc1_config,
    // )
    // .unwrap();

    // let mut delay = Delay::new(&clocks);

    // loop  {
    //     let pin_value: u16 = nb::block!(adc1.read(&mut pin)).unwrap();
    //     println!("PIN4 ADC reading = {}", pin_value);
    //     delay.delay_ms(1500u32);
    // }

    let gpio1 = Gpio::new(1, GpioState::IN);
    println!("Test");
    loop {}
}