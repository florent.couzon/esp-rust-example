#![no_std]

pub enum GpioState {
    NONE,
    IN,
    OUT,
    INOUT,
}

pub struct Gpio {
    number: i32,
    inout: GpioState,
    reg: i32,
}

const gpio_table: [i32; 4] = [2, 3, 4, 5];

pub trait GpioT {
    fn new(n: i32, inout: GpioState) -> Gpio;
}

impl GpioT for Gpio {
    fn new(n: i32, inout: GpioState) -> Gpio {
        Gpio {
            number: n,
            inout: inout,
            reg: gpio_table[n as usize],
        }
    }
}