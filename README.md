# Origin of the repo
As explained in src/README.rs the main part of the folder comes from a cloned git.
https://github.com/esp-rs/esp-template
It allows to start a small example on an ESP32 using Rust and the libraries built for it.
Example esp32-hal, esp-println

# What I do with it
This repo allows me to do tests on some driver implementation.
For now the goal is to implement a GPIO driver and mess with the microcontroller directly.
The file used is src/gpio.rs
The src/main.rs is currently used for tests and debug.